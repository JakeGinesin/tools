﻿module Bootstrap.Logging.DefaultLogSource

open ES.Fslog

/// Default logging source, which contains many useful logging methods. See
/// source for the methods it allows
let defaultLogSource (logProvider: LogProvider) : LogSource =
  let mn = $"{System.Environment.MachineName}"
  // Note: logging types, in increasing verbosity:
  // - Critical - These result in program failure
  // - Error - These may be recoverable, but will alter functionality
  // - Warning - These indicate something that may be wrong, but aren't
  //             obviously erroneous
  // - Info - General-purpose operations of the program
  // - Verbose - Fine-grained logging of the program
  log "Bootstrap"
  // Network-related logging
  |> critical "NetCritical"  $"{mn}: Network: {{0}}"
  |> error "NetErr"  $"{mn}: Network: {{0}}"
  |> warning "NetWarn"  $"{mn}: Network: {{0}}"
  |> info "NetInfo" $"{mn}: Ntwk: {{0}}\n"
  |> verbose "NetVerbose" $"{mn}: Ntwk: {{0}}\n"
  // Slave-related logging
  |> critical "SlaveCritical" "{0}"
  |> error "SlaveErr" "{0}"
  |> warning "SlaveWarn" "{0}"
  |> info "SlaveInfo" "{0}\n"
  |> verbose "SlaveVerbose" "{0}\n"
  // Status-related logging
  |> critical "Critical" $"{mn}: {{0}}"
  |> error "Error" $"{mn}: {{0}}"
  |> warning "Warning" $"{mn}: {{0}}"
  |> info "Info" $"{mn}: {{0}}\n"
  |> verbose "Verbose" $"{mn}: {{0}}\n"
  |> buildAndAdd logProvider