﻿module Bootstrap.CLI.Setup.AddConsoleLogger

open Argu
open Bootstrap.CLI.Arguments
open Bootstrap.CLI.Setup.Utils
open ES.Fslog
open ES.Fslog.Loggers

/// Add a ConsoleLogger to the logProvider, using the LogLevel obtained
/// from the ParseResults.
let addConsoleLogger
  (
    results: ParseResults<Arguments>,
    logProvider: LogProvider,
    logSource: LogSource
  ) : ParseResults<Arguments> * LogProvider * LogSource =
  let logLevel = getLogLevel results
  logProvider.AddLogger(new ConsoleLogger(logLevel))
  (results, logProvider, logSource)
