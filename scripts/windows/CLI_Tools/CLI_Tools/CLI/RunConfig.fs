﻿module Bootstrap.CLI.RunConfig

open Bootstrap.Servers.ServerStratum
open ES.Fslog

/// Runtime configuration created from 'Arguments'. This is used like the
/// Reader Monad to determine how facilities such as phone-home and outfile
/// logging should behave.
type RunConfig =
  {
    /// Sink for all of our logging needs. Handles the 'OutFile' and
    /// 'Listener' components, as well as any other logging facilities.
    log: LogSource
    /// The LogProvider the logSource drains into. This is not for general purpose
    /// use, and should only be used for adding loggers after initialization.
    _logProvider: LogProvider
    /// The role this server is, according to the provided arguments.
    stratum: ServerStratum
    /// What user we are running as. This is purely convenience.
    runningAs: string
    /// The name of the machine we are running on. This is purely convenience.
    machineName: string
    /// IP Address or hostname (NetBIOS) of the Master, if applicable.
    masterAddr: string
    /// The port our protocol is operating on.
    operatingPort: int
    /// Whether to skip the auto-propagation step. Primarily used for debugging.
    skipProp: bool
  }


