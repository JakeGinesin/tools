﻿module Bootstrap.Utils.Reader

/// Reader Monad
type Reader<'env,'a> = Reader of action:('env -> 'a)

/// Implementation of the Reader Monad. I didn't write this code. Shamelessly
/// stolen. 
/// Run a Reader with a given environment
let run env (Reader action)  = 
    action env  // simply call the inner function

/// Create a Reader which returns the environment itself
let ask = Reader id 

/// Map a function over a Reader 
let map f reader = 
    Reader (fun env -> f (run env reader))

/// Reader Monadic bind
let bind f reader =
    let newAction env =
        let x = run env reader 
        run env (f x)
    Reader newAction

/// Transform a Reader's environment from subtype to supertype.
let withEnv (f:'superEnv->'subEnv) reader = 
    Reader (fun superEnv -> (run (f superEnv) reader))  
    // The new Reader environment is now "superEnv"


/// Used to get the nice monadic interface
type ReaderBuilder() =
    member __.Return(x) = Reader (fun _ -> x)
    member __.Bind(x,f) = bind f x
    member __.Zero() = Reader (fun _ -> ())
    
    member __.While(guard: unit -> bool, body: Reader<'Env, 'T>): Reader<'Env, unit> =
        Reader (fun env -> 
          while guard() do
            run env body |> ignore
        )
    member __.For(list: seq<'T>, func: 'T -> Reader<'Env, 'A>): Reader<'Env, 'A> seq =
        list
        |> Seq.map (fun t ->
              let foo: Reader<'Env, 'A> = func t
              foo
            )
        // Reader (fun env ->
        //   let bar: seq<_> =
        //       list
        //       |> Seq.map (fun t ->
        //           let foo: Reader<'Env, 'A> = func t
        //           let ran = run env foo
        //           ran 
        //           ) // reader env u
        //   () :?> 'A
        // )
    member __.Combine(munit: Reader<'Env, unit>, ma: Reader<'Env, 'A>): Reader<'Env, 'A> =
        Reader (fun env ->
            run env munit
            run env ma)
    member __.Delay(func: unit -> Reader<'Env, 'A>): unit -> Reader<'Env, 'A> = func
/// Use this for the computation expression
let reader = ReaderBuilder()
