﻿module Bootstrap.Utils.Utils

open System
open System.Runtime.InteropServices

let (|Default|) = Option.defaultValue

/// <summary>
/// Implicit casting, for use with C# interop.
/// Example:
/// <code>
/// let win32Bool: Foundation.BOOL = ...
/// (* type error *) if win32Bool then ... else ...
/// (* type sound *) if (!> win32Bool) then ... else ...
/// </code>
/// </summary>
let inline (!>) (x:^a) : ^b = ((^a or ^b) : (static member op_Implicit : ^a -> ^b) x)
