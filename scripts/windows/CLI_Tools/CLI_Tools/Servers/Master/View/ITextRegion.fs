﻿module Bootstrap.Servers.Master.View.ITextRegion

open System
open Bootstrap.Servers.Master.IRpcReceiver

/// Used to prevent race conditions when moving the cursor on stdout
let _stdoutSync = new Object()

type Posn = int * int

/// A rectangular region of text to be used for drawing.
type ITextRegion =
  /// Width of the text region (i.e., the number of columns)
  abstract Width: int with get
  /// Height of the text region (i.e., the number of newlines)
  abstract Height: int with get
  abstract Posn: Posn with get, set
  /// <summary>
  /// Produce a string representing this text region. It should be <c>Width</c>
  /// units wide and <c>Height</c> units tall.
  /// </summary>
  abstract Render: unit -> Async<string>
  /// Draw the text region to an output of some kind, e.g. stdout
  abstract Draw: unit -> Async<unit>

