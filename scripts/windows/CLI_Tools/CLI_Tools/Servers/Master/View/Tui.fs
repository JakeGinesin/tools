﻿module Bootstrap.Servers.Master.View.Tui

open System
open Bootstrap.Servers.Master.Model.ISlaveModel
open Terminal.Gui

(*
Overarching plan for individual computers:

|--Computer Name---------------|
| Status: <(Dis)connected>     |
| Windows Version: <10/Server 2016/etc>
| <Button="Force GPUpdate">    |
| <Button="Send RPC">          |
| Logs: <Button="# logs">      |
|------------------------------|

Where the "# logs" button displays the total number of logs we have received.
Clicking on the button results in a modal window displaying a colored table of logs.

The "Send RPC" button opens a modal window (wizard?) to create an RPC to send.
*)
let computerWin(model: ISlaveModel ): Window =
  // let win = new Window(model.ComputerName, 2, null)
  let win = new Window(Title = model.ComputerName,
                       Width = Dim.Auto(),
                       Height = Dim.Auto())
  
  let statusLabel: View =
    new Label(Text = $"Status: {model.ConnectionStatus.Value}",
              X = Pos.Absolute 1,
              Y = Pos.Absolute 0)
  let windowsVersionLabel: View =
    new Label(Text = $"WinVer: {model.WindowsVersion.Value}",
              X = Pos.Left statusLabel,
              Y = Pos.Bottom statusLabel)
  
  let gpupdateButton: View =
    new Button(Text = "TODO: REMOTE GPUPDATE CALLBACK",
               X = Pos.Left statusLabel,
               Y = Pos.Bottom windowsVersionLabel)
  let rpcButton: View =
    new Button(Text = "Send RPC (TODO: RPC MODAL)",
               X = Pos.Left statusLabel,
               Y = Pos.Bottom gpupdateButton)
  let logsButton: View =
    new Button(Text = $"# logs (TODO: LOG STORAGE)",
               X = Pos.Left statusLabel,
               Y = Pos.Bottom rpcButton)
  
  win.Add([|
    statusLabel
    windowsVersionLabel
    gpupdateButton
    rpcButton
    logsButton
  |])
  
  // win.Height <- 6
  // win.Width <- 20
  
  // HACK: "react" to updates by querying the model upon redraws
  let mutable debug_count = 1
  let updateLabels _ = 
    statusLabel.Text <- $"Status: {model.ConnectionStatus.Value} ({debug_count})"
    statusLabel.NeedsDisplay <- true
    windowsVersionLabel.Text <- $"WinVer: {model.WindowsVersion.Value} ({debug_count})"
    windowsVersionLabel.NeedsDisplay <- true
    debug_count <- debug_count + 1
    true
    
  let _timeoutToken = Application.AddTimeout(TimeSpan.FromSeconds(1), updateLabels)
  // win.KeyUp |> Event.add (fun _ ->
  //   statusLabel.Text <- $"Status: {model.ConnectionStatus.Value} ({debug_count})"
  //   statusLabel.NeedsDisplay <- true
  //   windowsVersionLabel.Text <- $"WinVer: {model.WindowsVersion.Value} ({debug_count})"
  //   windowsVersionLabel.NeedsDisplay <- true
  //   debug_count <- debug_count + 1
  //   // printf $"Mouse event fired: winver: {model.WindowsVersion}"
  //   )
  
  win

let demo(models: ISlaveModel array): unit =
  Application.Init()
  let appWindow = new Window(Title = "Bootstrap - Master Control Panel",
                             BorderStyle = LineStyle.None)
  
  let mbItems: MenuBarItem array = [|
    new MenuBarItem ("_File", [| new MenuItem("_Quit (Kills Master)", "", fun () -> Application.RequestStop()) |])
  |]
  let menu = new MenuBar(Menus = mbItems)
  
  appWindow.Add(menu) |> ignore
  // Application.Top.Add(menu)
  
  let container =
    new FrameView(Title = "Machines",
                  X = Pos.AnchorEnd(),
                  Y = Pos.Absolute(1),
                  Width = Dim.Percent(100),
                  Height = Dim.Auto())
  container.Padding.Thickness <- Thickness(8, 1, 0, 0)
  
  let aligner = Aligner (AlignmentModes = AlignmentModes.StartToEnd)
  
  let modelsPerRow = 4
  let modelsPerColumn = 4
  
  let mutable _i = 0
  // let mutable names = ""
  models
  |> Array.map computerWin
  |> Array.map (fun win -> 
       win.X <- Pos.Align (aligner.Alignment, aligner.AlignmentModes, _i / modelsPerRow)
       win.Y <- Pos.Align (aligner.Alignment, aligner.AlignmentModes, _i % modelsPerColumn + 10)
       _i <- _i + 1
       // names <- names + $"{win.Title} "
       container.Add(win)
  )
  |> ignore
  
  // let debugLabel = new Label(Title = $"_i: {_i}, names: {names}",
  //                        X = Pos.Absolute 1,
  //                        Y = Pos.Bottom(container))
  
  appWindow.Add(container) |> ignore
  // Application.Top.Add(containerWin)
  // appWindow.Add(debugLabel) |> ignore
  Application.Run(appWindow)
  Application.Shutdown()