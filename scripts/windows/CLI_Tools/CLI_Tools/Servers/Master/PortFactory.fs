﻿module Bootstrap.Servers.Master.PortFactory

/////////////////////// Utility /////////////////////


/// Create free ports from a given starting port. Does not actually check if the
/// port is free.
type PortFactory private () =
  /// Note: RPC is on port 64790.
  static let defaultStartingPort = 60790
  static let mutable nextPortAssignment = defaultStartingPort

  /// Get a port to assign, incrementing by 1 for the next free port
  static member NextListenPort(): int =
    let ret = nextPortAssignment
    nextPortAssignment <- nextPortAssignment + 1
    ret

  /// Set the port to increment from. This probably doesn't need to be called often.
  static member SetStartingPort(port: int): unit =
    nextPortAssignment <- port

  /// Get the default port for RPC
  static member GetRpcPort(): int =
    64790