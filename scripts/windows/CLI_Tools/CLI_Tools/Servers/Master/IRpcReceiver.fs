﻿module Bootstrap.Servers.Master.IRpcReceiver

open WatsonTcp
open Bootstrap.Servers.RPC

/// Anything that can listen to and receive Rpcs. Used in 'RpcServer' to pass
/// messages to anything that can accept them.
type IRpcReceiver =
  abstract ReceiveLogMessage: LogRpc -> unit
  /// <summary>Handle a *response* to an Rpc, after it has been performed or 
  /// attempted.</summary>
  /// <param name="rpc">The Rpc that was performed or attempted.</param>
  /// <param name="result">The result of performing the Rpc.</param>
  abstract ReceiveControlMessage: rpc: ControlRpc -> result: string -> unit
  /// The initial Hello sent by the Slave.
  abstract ReceiveHelloMessage: hello: HelloRpc -> unit
  /// Upon connection, the ConnectionEventArgs are given to the receiver
  /// (e.g., SlaveConnection) to handle.
  abstract Connected: ConnectionEventArgs -> unit
  abstract Disconnected: DisconnectionEventArgs -> unit

/// Given to an IRpcReceiver in order to send Rpcs over the wire to a slave.
type IRpcAcceptor =
  /// Asynchronously send an Rpc to the given computer. 
  abstract SendRpc: rpc: ControlRpc -> computerName: string -> unit // TODO: should this return a handle for Rpcs in flight?
  /// Broadcast an Rpc to all connected computers.
  abstract BroadcastRpc: rpc: ControlRpc -> unit
  /// Register the given IRpcReceiver with the given ComputerName.
  abstract RegisterController: ComputerName: string -> RpcReceiver: IRpcReceiver -> unit
