﻿module Bootstrap.Servers.Master.Model.ISlaveModel

open Bootstrap.CLI.RunConfig
open Bootstrap.Logging.Pretty.PTree
open Bootstrap.Servers.Master.IRpcReceiver
open ES.Fslog
open FSharpPlus.Data
open Bootstrap.Servers.RPC

/// Abstract interface for a Model of a connection with a Slave.
/// The Model should do most the heavy lifting; a concrete Model (i.e., one
/// backed by an actual machine) should receive a callback from a
type ISlaveModel =
  // All SlaveModels must be able to handle network-related events; this is how
  // they react to disconnections, reconnections, incoming RPC-related non-logging
  // information.
  inherit IRpcReceiver

  /// The name of the Slave/Computer this Model represents.
  abstract member ComputerName: string 
  /// The status of the Slave's connection with the RpcListener
  abstract member ConnectionStatus: string ref
  /// The version of Windows the Slave is running. Preferably formatted in a
  /// Human readable format, e.g. "Windows Server 2019 (17763.3650)"
  /// This is likely obtained *after* initialization as the result of an RPC.
  abstract member WindowsVersion: string ref

  /// Run the bootstrapping sequence on the remote, likely using psexec64.
  /// If we are especially unlucky, we may need to use the 32 bit version.
  /// If this is the case, we're screwed because the cpu architecture
  /// is not currently checked.
  abstract BootstrapRemote: unit -> unit

  /// Attempt to retrieve the most recent log entry sent by the Slave. This
  /// could fail if the Slave has not connected to the LogListener and/or has not
  /// sent back any logs yet.
  abstract NewestLogEntry: Option<LogRpc>

  /// Attempt to retrieve the log history sent by the Slave. This could fail if
  /// the Slave has not connected to the LogListener and/or has not sent back
  /// any logs yet.
  abstract RetrieveLogHistory: unit -> Option<LogRpc seq>

  /// Get a prettified status of the Model. This should include the log
  abstract GetStatusPretty: unit -> PTree<string>