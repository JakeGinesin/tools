﻿module Servers.Hardening.HardeningAction

open Microsoft.Win32
open System.Security.AccessControl
open Bootstrap.Servers.Hardening.Serivces
open System.ServiceProcess
open System
open Windows.Win32
open Windows.Win32.System.Services

type AclModificationAction =
  /// Add the specified item(s), leaving the rest unchanged.
  | Add
  /// Remove the specified item(s).
  | Remove
  /// Remove all items, then add the specified items.
  | RemoveOthers
  /// Reset to the default/inherited ACL. Must be accompanied by a user to add
  /// after the reset. Makes the file protected and enables inheritance.
  | Reset

type FileModificationAction =
  /// Prepend the specified content to the file.
  | Prepend
  /// Append the specified content to the file.
  | Append
  /// Replace the file's content with the specified content.
  | Replace
  /// Find and replace the specified content in the file.
  | FindAndReplace of Filter: string

type HardeningAction =
  | RegistryModification
    of Key: string
     * Name: string
     * Value: obj
     * ValueKind: RegistryValueKind
     * ReplaceExisting: bool
  | FileModification
    of Path: string
     * Content: string
     * BackupExisting: bool
     * Action: FileModificationAction
  | FileAclModification // uses FileSecurity
    of Path: string
     * User: string
     * Rights: FileSystemRights // see: https://learn.microsoft.com/en-us/dotnet/api/system.security.accesscontrol.filesecurity?view=net-8.0
     * Action: AclModificationAction
  | ServiceModification
    of ServiceName: string
     * Action: ServiceAction
     * StartupMode: Option<Windows.Win32.System.Services.SERVICE_START_TYPE>
   /// Perform the actions in sequence. If any action fails, the entire sequence fails,
   /// and the remaining actions are not performed. The action will return as a failure,
   /// but the performed actions will not be rolled back.
  | And of Actions: HardeningAction seq
  /// Perform the first action that succeeds. If all actions fail, the entire sequence fails.
  /// The result will be a success if any action succeeds.
  | Or of Actions: HardeningAction seq

let rec performAction (action: HardeningAction): Result<unit, string> =
  match action with
  | RegistryModification (key, name, value, valueKind, replaceExisting) ->
    let regKey = Registry.LocalMachine.OpenSubKey(key, true)
    if regKey = null then
      Error (sprintf "Registry key %s does not exist" key)
    else
      try
        regKey.SetValue(name, value, valueKind)
        regKey.Close()
        Ok()
      with
      | ex ->
        regKey.Close()
        Error (sprintf "Failed to modify registry key %s: %s" key ex.Message)
  | FileModification (path, content, backupExisting, action) ->
    let fileExists = System.IO.File.Exists path
    if not fileExists then
      Error (sprintf "File %s does not exist" path)
    else
      let fileContent = System.IO.File.ReadAllText path
      let newContent =
        match action with
        | Prepend -> content + fileContent
        | Append -> fileContent + content
        | Replace -> content
        | FindAndReplace filter -> fileContent.Replace(filter, content)
      try
        if backupExisting then
          System.IO.File.Move(path, path + ".bak")
          // TODO: move backup file to the Master
        System.IO.File.WriteAllText(path, newContent)
        Ok()
      with
      | ex ->
        if backupExisting then
          System.IO.File.Move(path + ".bak", path)
          // TODO: move backup file to the Master
        Error (sprintf "Failed to modify file %s: %s" path ex.Message)
  | FileAclModification (path, user, rights, action) ->
    let fileExists = System.IO.File.Exists path
    if not fileExists then
      Error (sprintf "File %s does not exist" path)
    else
      try
        let fileSecurity = System.Security.AccessControl.FileSecurity(path, AccessControlSections.Access)
        let rule = FileSystemAccessRule(user, rights, AccessControlType.Allow)
        match action with
        | Add ->
          fileSecurity.AddAccessRule(rule)
          Ok()
        | Remove ->
          if fileSecurity.RemoveAccessRule(rule) then Ok()
          else Error (sprintf "Failed to remove ACL rule %s" path)
        | RemoveOthers ->
          fileSecurity.SetAccessRuleProtection(false, false)
          fileSecurity.SetAccessRule(rule)
          Ok()
        | Reset ->
          fileSecurity.ResetAccessRule(rule)
          fileSecurity.SetAccessRuleProtection(true, true)
          Ok()
      with
      | ex ->
        Error (sprintf "Failed to modify file ACL %s: %s" path ex.Message)
  | ServiceModification (serviceName, status, startupMode) ->
    let service = new ServiceController(serviceName)
    match status with
    | ServiceAction.Start ->
      try
        service.Start()
        Ok()
      with
      | ex ->
        Error (sprintf "Failed to start service %s: %s" serviceName ex.Message)
    | ServiceAction.Stop ->
      try
        service.Stop()
        Ok()
      with
      | ex ->
        Error (sprintf "Failed to stop service %s: %s" serviceName ex.Message)
    | ServiceAction.Restart ->
      try
        service.Stop()
        service.WaitForStatus(ServiceControllerStatus.Stopped)
        service.Start()
        Ok()
      with
      | ex ->
        Error (sprintf "Failed to restart service %s: %s" serviceName ex.Message)
    |> Result.bind (fun _ ->
        match startupMode with
        | None -> Ok()
        | Some mode -> setStartupMode serviceName mode
      )
  | And actions ->
    actions
    |> Seq.map performAction
    |> Seq.fold (fun acc res ->
        match acc, res with
        | Ok(), Ok() -> Ok()
        | Ok(), Error e -> Error e
        | Error e, Ok() -> Error e
        | Error e1, Error e2 -> Error (sprintf "%s\n%s" e1 e2)
      ) (Ok())
  | Or actions ->
    actions
    |> Seq.map performAction
    // collect all errors and report them:
    |> Seq.fold (fun acc res ->
        match res with
        | Ok() -> Ok()
        | Error e -> Error ($"{acc}\n{e}")
      ) (Error "All actions failed. Errors:\n")
