﻿module Bootstrap.Servers.Hardening.SMB

open Bootstrap.Servers.Hardening.Powershell

/// Disable SMBv1 Server on the machine. Returns whether the command was
/// Successful or not.
let disableSMBv1(): PwshResult =
  let command = @"Set-SmbServerConfiguration -EnableSMB1Protocol $false -Force"
  RunPowershellScript command FailOnNonZero
  
let RegisterSMBv1Removal(): (string * ScriptCb * list<WindowsVariant>) =
  "DisableSMBv1-Any", disableSMBv1, AnyVariant
