﻿module Bootstrap.Servers.Hardening.LocalUsers

open Bootstrap.Servers.Hardening.Powershell


/// Disable the Guest account on the machine.
let disableGuestAccount(): Result<unit, string> =
  let command = @"Get-LocalUser -Name Guest | Disable-LocalUser"
  RunPowershellScript command
  |> (fun { Result = output; Error = error} -> if output.Length = 0 then Ok() else Error error)

/// Disable the built-in Administrator account on the machine.
/// Matches any local user with admin in the name.
/// FIXME: need to create a new domain admin user, as disabling the local administrator account ALSO affects the domain administrator account
let disableBuiltInAdministrator(): Result<unit, string> =
  // let command = @"Get-LocalUser | Where-Object -Property Name -Matches 'Admin' | Disable-LocalUser"
  let command = ""
  RunPowershellScript command
  |> (fun { Result = output; Error = error} -> if output.Length = 0 then Ok() else Error error)
