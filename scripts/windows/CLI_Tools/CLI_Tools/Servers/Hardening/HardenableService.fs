﻿module Bootstrap.Servers.Hardening.HardenableService

/// Services for which we can easily harden. You would reach for these when
/// you need to add a service required by an inject (e.g., RDP or SSH), and
/// want a quick and easy hardening script to perform. Note that some of these
/// may change GPOs if applicable.
/// TODO: think of services that can be remotely hardened.
type HardenableService =
  | NeedToThinkOfThese