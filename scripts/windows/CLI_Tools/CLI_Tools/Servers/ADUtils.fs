﻿/// Utilities relevant to Active Directory that do not belong in Generic.fs
module Bootstrap.Servers.ADUtils

open System.DirectoryServices
open Bootstrap.Logging.Pretty.PTree
open FSharpPlus.Data
open FSharpPlus
open Bootstrap.CLI.RunConfig
open ES.Fslog

/// Get the Active Directory Domain the current system belongs to.
let getADDomain: ActiveDirectory.Domain =
  let dirContext =
    ActiveDirectory.DirectoryContext(
      ActiveDirectory.DirectoryContextType.Domain
    )

  let domain = ActiveDirectory.Domain.GetDomain(dirContext)
  domain

/// <summary> Make an LDAP request with a filter and a property to enumerate.
/// Based on https://stackoverflow.com/questions/1605567/list-all-computers-in-active-directory
/// To use the SearchResult, index the Properties field by the name of the
/// property: <code> 
///   EnumerateViaLdap "(objectClass=computer)" "name"
///   |> Seq.map (_.Properties["name"])
///   |> Seq.map (fun f -> f[0])
/// </code>
/// </summary>
let EnumerateViaLdap
  (filter: string)
  (property: string)
  : Reader<RunConfig, SearchResult seq> =
  monad {
    let! { log = logger } = ask
    let domain = getADDomain
    let address = $"LDAP://{domain.Name}"

    logger?Info <|
      P ("Enumerating Active Directory Forest",
         P "Method: LDAP",
         P $"Filter: {filter}",
         P $"Property: {property}")

    let entry = new DirectoryEntry(address) // FIXME: this should be a 'use' instead
    let searcher = new DirectorySearcher(entry)
    searcher.Filter <- filter
    searcher.SizeLimit <- 0 // return all entries
    searcher.PageSize <- 250 // 250 should be a safe number of entries per page
    // we only want the computer names
    searcher.PropertiesToLoad.Add(property) |> ignore

    return (searcher.FindAll() |> Seq.cast<SearchResult>)
  }


/// Enumerate the computer objects that exist in the Active Directory domain,
/// excluding this computer.
let domainComputers: Reader<RunConfig, SearchResult seq> =
  EnumerateViaLdap "(objectClass=computer)" "name"
