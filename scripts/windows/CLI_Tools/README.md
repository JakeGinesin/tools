﻿
### Potential areas for improvement:
  #### Space
    - Use Mono? --> I have a nix expression that builds a minimal version of Mono5
    - Replace TcpClient and TcpListener with a homebrew socket-based solution
    - Replace argument parsing library with something that doesn't use reflection
    - Relocate the Master code and Slave code into different binaries
    - (Hard) Replace logging library with a homebrew version
