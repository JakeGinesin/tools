This is a Microsoft-provided hardening script for Windows 10 1809 (Redstone 2), Windows Server 2019, 
for both domain-joined and non-domain-joined variants. Run the script in Local_Script with an
Administrator-level powershell session to apply the hardening configuration. Note that this may enable
bitlocker... and a number of unwanted bitlocker settings.
