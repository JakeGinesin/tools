# Path to Sysinternals
$SysinternalsPath = "C:\sysinternals"

# install sysinternals
Start-Process powershell.exe -ArgumentList "-File .\install-sysinternals.ps1"

# Open Sysinternals tools
Start-Process -FilePath "$SysinternalsPath\tcpview64.exe"
Start-Process -FilePath "$SysinternalsPath\autoruns64.exe"
Start-Process -FilePath "$SysinternalsPath\procexp64.exe"

# Run the rest

# windows defender script
Start-Process powershell.exe -ArgumentList "-File .\big-harden.ps1"

# see whatz goin on
Start-Process powershell.exe -ArgumentList "-File .\information.bat"

# check for amogus activity
Start-Process powershell.exe -ArgumentList "-File .\susServices.ps1"